# coding: utf-8
class ConsultarScreen < AndroidScreenBase

  # Identificador da tela
  trait(:trait)                 { "* id:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)         { 'consultar_activity' }
  element(:campo_cep)           { 'input_cep' }
  element(:campo_buscar)        { 'botao_consulta' }

  action(:tocar_botao_buscar) {
    touch_screen_element campo_buscar
  }

  # @param [String] cep
  def digitar_cep cep
    enter cep, campo_cep
  end

end
