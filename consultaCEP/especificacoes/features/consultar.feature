# language: pt
Funcionalidade: Consultar
  Eu como um usuário posso consultar um CEP válido.
  Não temos controle de tipos de usuário.
  Se consultar um CEP inválido (problema de formatação), devo ver uma mensagem de erro.
  Se consultar um CEP inexistente (CEP válido que não existe na base dos Correios), devo ver uma mensagem de erro.

  Cenário: Posso consultar um CEP válido
    Dado que estou na tela de consulta de CEP
    Quando digitar um CEP válido
    E tocar o botão buscar
    Então poderei ver o endereço do CEP válido

  Cenário: Posso consultar um CEP válido
    Dado que estou na tela de consulta de CEP
    Quando digitar um CEP inválido
    E tocar o botão buscar
    Então poderei ver a mensagem do CEP inválido

  Cenário: Posso consultar um CEP válido
    Dado que estou na tela de consulta de CEP
    Quando digitar um CEP inexistente
    E tocar o botão buscar
    Então poderei ver a mensagem do CEP inexistente