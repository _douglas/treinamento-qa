class Calculadora

  # @param [Fixnum] primeiro_numero
  # @param [Fixnum] segundo_numero
  # @return [Fixnum]
  def soma primeiro_numero, segundo_numero
    return primeiro_numero + segundo_numero
  end

  # @param [Fixnum] divisor
  # @param [Fixnum] dividendo
  # @return [Fixnum]
  def divisao dividendo, divisor
    return dividendo / divisor
  end

end
