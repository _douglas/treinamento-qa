# Treinamento de QA da Concrete Solutions

Treinamento ministrado por Oscar Tanner.

Para realizar todas as atividades do treinamento, sua máquina deve estar configurada com:

- Ambiente de desenvolvimento Android;
- Ambiente de desenvolvimento iOS;
- Ruby instalado;
- Gems instaladas: calabash-cucumber (iOS), calabash-android (Android), calabash-common e cs-bdd;
- ios-deploy instalado;
- Um editor de texto.

[Aqui](https://gist.github.com/CSOscarTanner/95669dff666e0efc0afa) você encontra um passo a passo de como configurar a sua máquina.

### Primeiro dia de treinamento.

  Clique [aqui](https://www.youtube.com/watch?v=mRrAAnI-gW8) para ver o vídeo do primeiro dia.

**Atenção:**

 * **00:17:15**: início do treinamento;
 * **01:35:13**: pausa para pizza;
 * **02:01:55**: fim da pausa e reinício do treinamento.

### Segundo dia de treinamento

Clique [aqui](https://www.youtube.com/watch?v=nqjbTCtp8JI) para ver o vídeo do segundo dia.

**Atenção**

* **00:16:00**: início do treinamento;
* **01:05:25**: pausa para pizza;
* **01:36:12**: fim da pausa e reinício do treinamento


### Alguns links úteis:

> [A importância do QA para o Desenvolvimento Mobile](http://blog.concretesolutions.com.br/2014/10/a-importancia-do-qa-para-o-desenvolvimento-mobile/)

> [Breve introdução a BDD e Cucumber](http://blog.concretesolutions.com.br/2014/12/introducao-bdd-e-cucumber/)

> [Automatizando testes funcionais no Android com Calabash](http://blog.concretesolutions.com.br/2014/12/automatizando-testes-funcionais-no-android-com-calabash/)

> [Automatizando testes funcionais no iOS com Calabash](http://blog.concretesolutions.com.br/2015/01/automatizando-testes-ios-com-calabash/)

> [Automatizando testes funcionais no Android e no iOS com Calabash](http://blog.concretesolutions.com.br/2015/05/testes-funcionais-android-e-ios-calabash/)

> [Boas práticas de Cucumber e Calabash – Parte 1](http://blog.concretesolutions.com.br/2015/07/cucumber-e-calabash-parte-1/)

> [Is QA Dead?](https://medium.com/featured-insights/is-qa-dead-f1aa21b3f8b8)

###Breve descrição dos tipos de testes apresentados

 - **TESTES CAIXA PRETA:** Testes elaborados a partir das funcionalidades e requisitos do sistema. A estrutura interna do sistema não é considerada na elaboração dos testes. Esse tipo de testes tem como objetivo encontrar problemas com a utilização indevida do sistema.


 - **TESTES CAIXA BRANCA:** Testes elaborados a partir do conhecimento da estrutura interna de um software e da habilidade de programação do profissional de testes. Nesses tipos de testes a funcionalidade não é considerada, sendo levado em conta o funcionamento do código fonte de acordo com diferentes valores inseridos pelo testador, que deseja exercitar os diferentes caminhos lógicos presentes dentro do sistema.


 - **TESTES CAIXA CINZA:** Uma mistura dos testes Caixa Branca e Caixa Preta que tem como objetivo encontrar tanto problemas com a estrutura interna do sistema, quanto com a sua utilização indevida.


 - **TESTES DE UNIDADE:** Tem como objetivo testar individualmente estruturas internas do software, como métodos, classes, componentes, módulos, entre outros. Normalmente realizado pelos desenvolvedores por necessitar de grande conhecimento da estrutura interna do software.


 - **TESTES DE USABILIDADE:** Tem como objetivo testar o quanto a interface ou fluxo do sistema é amigável para o usuário.


 - **TESTES FUNCIONAIS:** Tem como objetivo validar se as especificações do sistema foram realmente implementadas. Esses testes são realizados por meio da análise das respostas fornecidas de acordo com o tipo de entrada inserida. Resumindo: foca-se no que o sistema deve fazer e não em como as funcionalidades foram implementadas.


 - **TESTES DE ACEITAÇÃO:** Muito parecido com os testes funcionais, porém normalmente é executado pelo Usuário ou Cliente, que tem como objetivo verificar se as funcionalidades implementadas realmente atendem o que era esperado.


 - **TESTES DE REGRESSÃO:** Tem como objetivo testar totalmente o software de forma a garantir que nenhuma de suas funcionalidades parou de funcionar.


 - **TESTES DE INTEGRAÇÃO:** Tem como objetivo testar se como as diferentes partes do sistema se comportam em conjunto após a sua integração.


 - **TESTES DE CARGA:** Tem como objetivo verificar o comportamento do sistema quando ele está bastante carregado.


 - **TESTES DE STRESS:** Tem como objetivo sobrecarregar o sistema para verificar quando e como ele falha. Exemplos de testes: tentar armazenar mais informação que o sistema de armazenamento suporta, queries de BD complexas, entre outras.


 - **TESTES DE PERFORMANCE:** Tem como objetivo analisar a performance do sistema em relação a disponibilidade e estabilidade quando submetido a uma determinada carga.


 - **TESTES AUTOMATIZADOS:** A partir da utilização de uma ferramenta, framework ou software pode-se realizar algum tipo de testes automaticamente e sem a interação de um humano.Tipos de Testes.


### Consulta CEP

Para testarmos o funcionamento do Calabash, vamos utilizar um aplicativo conhecido por muitos da Concrete, o ConsultaCEP.

O Victor Nascimento e o Alexandre Garrefa gentilmente forneceram um exemplo de implementação desse APP. O APP Android está disponível [aqui](https://github.com/cs-victor-nascimento/consultaCEP) e o iOS [aqui](https://bitbucket.org/cs_garrefa/desafio-ios)

O projeto com as especificações pode ser encontrado neste mesmo repositório na pasta consultaCEP. Como para a execução dos teste precisamos somente dos aplicativos (.app ou .apk) eles também se encontram na pasta consultaCEP.

> Caso não use os apps, mas sim os projetos, lembre-se de:

> - Adicionar o id "consultar_activity" no arquivo "app/src/main/res/layout/activity_pesquisa.xml" do projeto Android.

> - Adicionar os accessibilityIdentifier "CONSULTAR\_SCREEN" e "CAMPO\_CEP" no arquivo ConcreteTest/IphoneStoryboard.storyboard do projeto iOS

### Referências

Algumas das referências utilizadas para montar a apresentação

> Types of software testing: [http://www.softwaretestinghelp.com/types-of-software-testing/](http://www.softwaretestinghelp.com/types-of-software-testing/)

> Test Driven Development: TDD simples e prático: [http://www.devmedia.com.br/test-driven-development-tdd-simples-e-pratico/18533](http://www.devmedia.com.br/test-driven-development-tdd-simples-e-pratico/18533)

> Behaviour-driven development: [http://dannorth.net/introducing-bdd/](http://dannorth.net/introducing-bdd/)

> ADZIC, Gojko. Título: Specification By Example. Segunda Edição. United States of America: Manning, August 2012. 270p.

> Calabash: [http://developer.xamarin.com/guides/testcloud/calabash/introduction-to-calabash/](http://developer.xamarin.com/guides/testcloud/calabash/introduction-to-calabash/)